### STAGE 1: Build ###
FROM node:12.2.0


WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 7777

# Run the specified command within the container.
CMD [ "npm", "start" ]

#RUN npm run build
#RUN npm start
