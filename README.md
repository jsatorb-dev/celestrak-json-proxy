# celestrak-proxy

## Overview
This applications is a proxy server between client and celestrak.com.
It convert celelstrak output into JSON and allow CORS.

nodejs Express framework has been used to code this app.

## Example 
To access to:
http://celestrak.com/NORAD/elements/active.txt

you need to use this:
http://localhost:7777/NORAD/elements/active.txt

##INSTALL and RUN
```bash
    npm install 
    npm start
```

Test address redirection http://localhost:7777/NORAD/elements/active.txt in a browser


## Docker

# Build docker image (tag: celestrak-json-proxy:dev)
```bash
docker build -t celestrak-json-proxy:dev .
```



# Run a docker container (name: celestrak-json-proxy-container)

```bash
docker run -it --name celestrak-json-proxy-container -p 7777:7777 --rm celestrak-json-proxy:dev
```

-p 7777:7777 exposes port 7777 to other Docker containers on the same network (for inter-container communication) and port 7777 to the host.
--rm removes the container and volumes after the container exits.


use -d flag to run the container in the background
