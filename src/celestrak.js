const fs = require('fs');
module.exports =  {
     tleTxt2JSON(txtTle){
        lines = txtTle.split('\r\n');
        const tleJson = [];
        for(let i = 0; i < lines.length; i += 3){
            tleJson.push({
                "name": String(lines[i]).replace(/\s+/g, ' ').trim(),
                "line1": lines[i+1],
                "line2": lines[i+2],
            });
        }
        return tleJson;
    }
}