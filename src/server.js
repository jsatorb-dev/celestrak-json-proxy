const express = require( "express" );
const app = express();
const request = require('request');
const celestrak = require('./celestrak');

const port = 7777; // default port to listen


app.use(function(req, res, next) {
    /**
     * Express middlewareAllow all CORS requests on the app proxy
     */
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.use(function (req, res, next) {
    /**
     * Express middleware that read all request, to make the same to satsearch API and transmit response
     */
    const options = {
        url: 'http://www.celestrak.com' + req.url
      };
    request(options, (error, response, body) => {
        try{
            res.setHeader('Content-Type', 'application/json');
            
            res.send(celestrak.tleTxt2JSON(body));
        } catch (e) {
            res.status(500).send('This url is unknown or does not return a tle list');
        }
    });
  });
// start the Express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );